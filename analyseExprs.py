from collections import defaultdict
from itertools import chain, combinations, permutations
import numpy as np
import seg

geneNms = [
    'AP3',
    'REV',
    'SUP',
    'AHP6',
    'PHB_PHV',
    'SEP1_2',
    'PI',
    'LFY',
    'AS1',
    'AG',
    'ATML1',
    'STM',
    'AP1_2',
    'L2',
    'L1',
    'ETTIN',
    'SEP3',
    'FIL',
    'WUS',
    'PUCHI',
    'ANT',
    'CUC1_2_3',
    'MP',
    'CLV3',
    ]

def readData(d, dExprs):
    from operator import add
    from functools import reduce
    from os.path import join

    cells = []
    
    timepoints = [10, 40, 96, 120, 132]
    geomF = "_segmented_tvformat_0_volume_position.txt"
    neighF = "_segmented_tvformat_0_neighbors_py.txt"
    exprF = "h.txt"

    for t in timepoints:
        print t
        fnGeom = join(d, "t" + str(t) + geomF)
        fnNeigh = join(d, "t" + str(t) + neighF)
        fnExpr = join(dExprs, "t_" + str(t) + exprF)

        gexprs = seg.readExprs(fnExpr)
        ts = seg.STissue.fromTV(fnGeom, fnNeigh, gexprs, seg.geneNms)
        cells.append(list(iter(ts)))

    return reduce(add, cells)

def initPs(geneNms, val):
    ps = dict()
    for (g1, g2) in combinations(geneNms, 2):
        ps[frozenset({g1, g2})] = val

    return ps

def initOrd2(geneNms, val):
    ps = defaultdict(dict)

    for (g1, g2) in permutations(geneNms, 2):
        ps[g1][g2] = val

    return ps

def ps12(cells):
    ps1 = dict((g, np.zeros(2)) for g in geneNms)     #Map String Vec2
    ps2 = initPs(geneNms, np.zeros((2, 2)))           #Map (Set String) Mat22

    N = 0
    
    for cell in cells:
        N += 1
        for g in geneNms:
            ps1[g][int(cell.exprs[g])] += 1 
        
        for (g1, g2) in combinations(geneNms, 2):
            counts12 = ps2[frozenset({g1, g2})]
            counts12[int(cell.exprs[g1])][int(cell.exprs[g2])] += 1

    for g in geneNms:
        ps1[g] /= N

    for (g1, g2) in combinations(geneNms, 2):
        ps2[frozenset({g1, g2})] /= N
        
    return (ps1, ps2)

def entropy(P):
    return -np.sum(P * np.log(P))

def mkEntropies(ps1, ps2)
    H1 = dict((g, 0.0) for g in geneNms)     #Map String Float
    H2 = initPs(geneNms, 0.0)                #Map (Set String) Float

    for g in geneNms:
        H1[g] = entropy(ps1[g])

    for (g1, g2) in combinations(geneNms, 2):
        H2[frozenset({g1, g2})] = entropy(ps2[frozenset({g1, g2})])

    return (H1, H2)

def analyseEntropies(H1, H2):
    #essentially try to find dependencies
    #if M(A, B) = H(A) then A->B
    #Now M(A, B) is directional so we don't use set
    #M(A, B): Map String (Map String Float)
    #we need 2-permutations here instead of 2-combinations
    M = initOrd2(geneNms, 0.0)
    
    for g1, g2 in permutations(geneNms, 2):
        M[g1][g2] = H1[g1] + H1[g2] - H2[frozenset({g1, g2})]

    #for each pair of genes if M(A, B) = H(A) then
    for g1, g2 in permutations(geneNms, 2):
        if M[g1][g2] == H1[g1]:
            print g1 + "--->" + g2      #is this the correct direction???

    return M
