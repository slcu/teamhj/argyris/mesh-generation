import generate_mesh as gm
import os.path

def go():
    fs = ["/home/argyris/home3/genes2shape/flowerMeshes/flowerAtlas/t40/t40.tif",
          "/home/argyris/home3/genes2shape/flowerMeshes/flowerAtlas/t96/t96.tif",
          "/home/argyris/home3/genes2shape/flowerMeshes/flowerAtlas/t120/t120.tif",
          "/home/argyris/home3/genes2shape/flowerMeshes/flowerAtlas/t132/t132.tif"]

    fs1 = ["/home/argyris/home3/genes2shape/flowerMeshes/mechPerturbations/180926_day 3_T5_Myr-yfp-ory treated.lif-ory-mock treated-p3.tif"]
    for fin in fs:
        print fin

        gm.mkMeshFromFile(fin, 2500)

    return

if __name__ == "__main__":
    go()
    
