from skimage.measure import regionprops
import phenotastic.file_processing as fp
import numpy as np

def tupleToString(t):
    return " ".join([str(p) for p in t])

def extractCellCtr(cell):
    return (cell['label'], ) + cell['centroid']

def readCells(fn):
    f = fp.tiffload(fn)
    seg = f.data.astype(int)
    cells = regionprops(seg)
    
    return [extractCellCtr(cell) for cell in cells]

def writeCentres(centres, fnOut):
    with open(fnOut, 'w') as fout:
        fout.writelines("\n".join([tupleToString(ctr)
                                   for ctr in centres]))

    return
