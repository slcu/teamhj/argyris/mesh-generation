import numpy as np
import os

def normNeigh(fn):
    (nm, ext) = os.path.splitext(fn)
    content = ""
    with open(fn, 'r') as f:
        content = f.read()
        content = content.replace("cid", "{\"cid\"").replace("neighbors ids", "\"neighbors ids\"").replace("]", "]}")

    fout = nm + "_py" + ext
    with open(fout, 'w+') as f:
        f.write(content)

    return

def norm(d):
    timepoints = [10, 40, 96, 120, 132]

    for t in timepoints:
        fn = "t" + str(t) + "_segmented_tvformat_0_neighbors.txt"
        print fn
        normNeigh(os.path.join(d, fn))

    return

def readHeader(fn):
    with open(fn) as f:
        geneNames = f.readline().rstrip().split(' ')[1:]

    return set(geneNames)

def getGeneNames(d):
    from os import listdir
    from os.path import isfile, join
    
    return set.union(*[readHeader(join(d, fn)) for fn in listdir(d) if isfile(join(d, fn))])

def mkColours(N):
    return [tuple(np.random.choice(range(256), size=3)) for i in range(N)]

def toXmlCol(col, i):
    #i: int 
    #col: (Num, Num, Num)
    #returns: string

    (r, g, b) = col

    return "<Point x=\"%d\" o=\"1\" r=\"%d\" g=\"%d\" b=\"%d\"/>" % (i, r, g, b)

def toXmlCMap(N, cols):
    pre = ["<ColorMaps>", "<ColorMap name=\"Random Blue\" space=\"RGB\">"]
    colsXml = [toXmlCol(c, i) for (i, c) in zip(range(N), cols)]
    post = ["</ColorMap>", "</ColorMaps>"]

    return "\n".join(pre+colsXml+post)


def mkRandCMap(N, fout):
    cols = mkColours(N)

    with open(fout, 'w') as f:
        f.write(toXmlCMap(N, cols))

    return

def go():
    N = 500
    mkRandCMap(N, "randBlue.xml")
                
                    
