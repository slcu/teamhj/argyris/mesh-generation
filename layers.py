from collections import defaultdict

def parseComment(f):
    f.readline()
    return

def parseHeader(f):
    f.readline()
    return

def parseLine(ln):
    ln = ln.rstrip()
    (tp, rest) = ln.split(',')
    (cid, layer) = rest.split(':')

    return (int(tp), int(cid), layer)

def dlayer(d):
    return d[2]

def dtp(d):
    return d[0]

def dcid(d):
    return d[1]

def initDict():
    layers = dict()
    for tp in range(18):
        layers[tp] = dict([(layer, []) for layer in ['L1', 'L2', 'L3']])

    return layers

def parseLayersFile(fn):
    layers = initDict()
    
    f = open(fn, 'r')
    
    parseComment(f)
    parseHeader(f)

    data = [parseLine(ln) for ln in f]

    for d in data:
        layers[dtp(d)][dlayer(d)].append(dcid(d))

    return layers
